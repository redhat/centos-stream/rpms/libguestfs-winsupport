#!/bin/bash -
set -e
set -x

# Fix libvirt.
systemctl restart libvirtd

# Create an NTFS filesystem and mount.
guestfish -vx <<EOF
set-program "virt-foo"
sparse test.img 1G
run
part-disk /dev/sda mbr
mkfs ntfs /dev/sda1
mount /dev/sda1 /
touch /tested
umount-all
EOF
